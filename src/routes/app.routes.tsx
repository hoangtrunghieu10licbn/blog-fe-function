import { lazy } from 'react';
const Profile = lazy(() => import('../pages/Profile'));
const PostList = lazy(() => import('../pages/Home/Post/PostList'));
const PostDetail = lazy(() => import('../pages/Home/Post/PostDetail'));

const routes = [
  {
    path: '/home',
    component: PostList,
  },
  {
    path: '/post-detail/:id',
    component: PostDetail,
  },
  {
    path: '/profile',
    component: Profile,
  },
];

export default routes;
