import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

function makeHttpRequest(request: AxiosRequestConfig): Promise<AxiosResponse<any>> {
  return new Promise(async (resolve, reject) => {
    try {
      await axios(request)
        .then((response: AxiosResponse<any>) => {
          if (!(response.status >= 200 && response.status < 300)) {
            throw new Error(`${request.method} ${request.url} status: ${response.status}`);
          }
          return resolve(response);
        })
        .catch(err => {
          return reject(err);
        });
    } catch (err) {
      console.log('er', err);
      return reject(err);
    }
  });
}

export default makeHttpRequest;
