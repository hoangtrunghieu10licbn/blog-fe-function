export interface Author {
  _id: string;
  name: string;
  avatar: string;
}

export interface Post {
  _id: string;
  title: string;
  content: string;
  image_urls: string[];
  is_liked: boolean;
  total_like: number;
  create_at: Date;
  update_at: Date;
  author: Author;
}

export interface User {
  _id: string;
  email: string;
  name: string;
  sex: string;
  avatar: string;
  address: string;
  phone_number: string;
}

export interface SimplifiedUser {
  _id: string;
  name: string;
  avatar: string;
}
