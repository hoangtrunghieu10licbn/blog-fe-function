import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { User } from 'types';

interface RequestData {
  password: string;
  email: string;
}

interface Request extends AxiosRequestConfig {
  data: RequestData;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  access_token: string;
  token_type: string;
  expired_in: number;
  user: User;
}

const login = async function(args: RequestData): Promise<ResponseData> {
  try {
    const request: Request = {
      method: 'POST',
      url: `${SERVER_URI}/api/auth/login`,
      data: {
        ...args,
      },
    };

    const result: AxiosResponse<ResponseData> = await makeHttpRequest(request);

    return result.data;
  } catch (e) {
    return e;
  }
};
export default login;
