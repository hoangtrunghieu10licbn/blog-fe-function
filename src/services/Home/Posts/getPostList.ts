import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';
import { Post } from 'types';

const cookies = new Cookies();

interface RequestData {
  offset?: number;
  limit?: number;
  author?: string;
}

interface Request extends AxiosRequestConfig {
  params: RequestData;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  total: number;
  posts: Post[];
}

const getPostList = async function(args: RequestData): Promise<ResponseData> {
  try {
    const { offset, limit, author } = args;

    const accessToken: string = cookies.get('accessToken') || '';

    const request: Request = {
      method: 'GET',
      url: `${SERVER_URI}/api/posts`,
      params: {
        offset,
        limit,
        author,
      },
      headers: {
        access_token: accessToken,
      },
    };

    const result: AxiosResponse<ResponseData> = await makeHttpRequest(request);

    return result.data;
  } catch (e) {
    return e;
  }
};

export default getPostList;
