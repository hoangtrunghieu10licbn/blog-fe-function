import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';
import { SimplifiedUser } from 'types';

const cookies = new Cookies();

interface RequestData {
  post: string;
  content: string;
}

interface Request extends AxiosRequestConfig {
  data: RequestData;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  _id: string;
  content: string;
  user: SimplifiedUser;
}

const addComment = async function(args: RequestData): Promise<ResponseData> {
  try {
    const accessToken: string = cookies.get('accessToken') || '';

    const request: Request = {
      method: 'POST',
      url: `${SERVER_URI}/api/comment`,
      data: {
        ...args,
      },
      headers: {
        access_token: accessToken,
      },
    };

    const result: AxiosResponse<ResponseData> = await makeHttpRequest(request);

    return result.data;
  } catch (e) {
    return e;
  }
};
export default addComment;
