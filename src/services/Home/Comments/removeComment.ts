import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';

const cookies = new Cookies();

interface RequestData {
  id: string;
}

interface ResponseData {
  err: number;
  msg: string;
}

const removeComment = async function(args: RequestData): Promise<ResponseData> {
  try {
    const { id } = args;
    const accessToken: string = cookies.get('accessToken') || '';

    const result: AxiosResponse<ResponseData> = await makeHttpRequest({
      method: 'DELETE',
      url: `${SERVER_URI}/api/comment/${id}`,
      headers: {
        access_token: accessToken,
      },
    });

    return result.data;
  } catch (e) {
    return e;
  }
};
export default removeComment;
