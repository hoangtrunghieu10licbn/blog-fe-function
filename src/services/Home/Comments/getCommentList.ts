import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';
import { SimplifiedUser } from 'types';

const cookies = new Cookies();

interface RequestData {
  post: string;
  offset?: number;
  limit?: number;
}

interface Request extends AxiosRequestConfig {
  params: RequestData;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  total: number;
  comments: Comment[];
}

interface Comment {
  _id: string;
  content: string;
  created_at: Date;
  user: SimplifiedUser;
}

const getCommentList = async function(args: RequestData): Promise<ResponseData> {
  try {
    const accessToken: string = cookies.get('accessToken') || '';

    const request: Request = {
      method: 'GET',
      url: `${SERVER_URI}/api/comments`,
      params: {
        ...args,
      },
      headers: {
        access_token: accessToken,
      },
    };

    const result: AxiosResponse<ResponseData> = await makeHttpRequest(request);

    return result.data;
  } catch (e) {
    return e;
  }
};

export default getCommentList;
