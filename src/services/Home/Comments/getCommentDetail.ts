import makeHttpRequest from 'helpers/MakeRequest.helper';
import { SERVER_URI } from 'constants/index';
import { AxiosResponse } from 'axios';
import { Cookies } from 'react-cookie';
import { SimplifiedUser } from 'types';

const cookies = new Cookies();

interface RequestData {
  id: string;
}

interface ResponseData {
  err: number;
  msg: string;
  data: Data;
}

interface Data {
  _id: string;
  content: string;
  user: SimplifiedUser[];
}

const getCommentDetail = async function(args: RequestData): Promise<ResponseData> {
  try {
    const { id } = args;

    const accessToken: string = cookies.get('accessToken') || '';

    const result: AxiosResponse<ResponseData> = await makeHttpRequest({
      method: 'GET',
      url: `${SERVER_URI}/api/comment`,
      params: {
        id,
      },
      headers: {
        access_token: accessToken,
      },
    });

    return result.data;
  } catch (e) {
    return e;
  }
};

export default getCommentDetail;
