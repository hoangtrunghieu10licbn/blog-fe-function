import React, { Component, lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import ErrorBoundary from './components/ErrorBoudaries';
import FallbackLoading from './components/FallbackLoading';

const Login = lazy(() => import('./pages/Login'));
const Register = lazy(() => import('./pages/Register'));
const Blog = lazy(() => import('./pages/Blog'));

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
        <Suspense fallback={<FallbackLoading />}>
          <Switch>
            <Route path="/login" name="Login" component={Login} />
            <Route path="/register" name="Register" component={Register} />
            <Route path="/" name="Blog" component={Blog} />
          </Switch>
        </Suspense>
      </ErrorBoundary>
    );
  }
}

export default App;
