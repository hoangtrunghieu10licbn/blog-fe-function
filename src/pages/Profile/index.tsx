import React, { Component } from 'react';
import QueueAnim from 'rc-queue-anim';

class Page extends Component {
  render() {
    return (
      <QueueAnim type={['right', 'left']}>
        <div key="0">
          <div>Profile</div>
        </div>
      </QueueAnim>
    );
  }
}

export default Page;
