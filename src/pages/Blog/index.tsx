import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from 'layout';

class Blog extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" component={Layout}></Route>
      </Switch>
    );
  }
}

export default Blog;
