import React, { FunctionComponent } from 'react';
import QueueAnim from 'rc-queue-anim';
import Register from './components/Register';

const Page: FunctionComponent = () => {
  return (
    <QueueAnim type={['right', 'left']}>
      <div key="0">
        <Register />
      </div>
    </QueueAnim>
  );
};

export default Page;
