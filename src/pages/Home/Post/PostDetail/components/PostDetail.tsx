import React, { Component } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Row, Col, Card, Icon, Avatar, notification, Typography, Spin, Modal } from 'antd';
import { getPostDetail, removePost } from 'services/Home/Posts';
import { Post } from 'types';
import { SERVER_URI } from 'constants/index';
import { Cookies } from 'react-cookie';
import UpdatePostForm from './UpdatePostForm';
import './PostDetail.css';

const cookies = new Cookies();
const { confirm } = Modal;
const { Title, Paragraph } = Typography;

interface State {
  loading: boolean;
  post: Post | null;
  visibleUpdatePostForm: boolean;
}

class PostDetail extends Component<RouteComponentProps<{ id: string }>, State> {
  constructor(props: RouteComponentProps<{ id: string }>) {
    super(props);

    this.state = {
      loading: false,
      post: null,
      visibleUpdatePostForm: false,
    };
  }

  componentDidMount() {
    this.doGetPostDetail();
  }

  doGetPostDetail = async () => {
    try {
      this.setState({
        loading: true,
      });

      const result = await getPostDetail({
        id: this.props.match.params.id,
      });

      console.log(`result`, result);

      if (result.err !== 0) {
        notification.error({
          message: 'Something went wrong!',
          description: `${result.msg}`,
        });
        return;
      }

      this.setState({
        post: result.data,
      });
    } catch (e) {
      notification.error({
        message: `Something went wrong!`,
        description: `${e.toString()}`,
      });
    } finally {
      this.setState({
        loading: false,
      });
    }
  };

  handleDeleteConfirm = () => {
    const doRemovePost = async () => {
      try {
        this.setState({
          loading: true,
        });

        const result = await removePost({
          id: this.props.match.params.id,
        });

        console.log(`result`, result);

        if (result.err !== 0) {
          notification.error({
            message: 'Something went wrong!',
            description: `${result.msg}`,
          });
          return;
        }

        this.props.history.push('/home');
      } catch (e) {
        notification.error({
          message: `Something went wrong!`,
          description: `${e.toString()}`,
        });
      } finally {
        this.setState({
          loading: false,
        });
      }
    };

    confirm({
      title: 'Are you sure delete this post?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        console.log('OK');
        doRemovePost();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  showUpdatePostForm = () => {
    this.setState({
      visibleUpdatePostForm: true,
    });
  };

  hideUpdatePostForm = () => {
    this.setState({
      visibleUpdatePostForm: false,
    });
  };

  handleUpdatePostSuccessfully = () => {
    this.setState({
      visibleUpdatePostForm: false,
    });

    this.doGetPostDetail();
  };

  render() {
    const { post, loading, visibleUpdatePostForm } = this.state;

    const isLoggedIn = !!cookies.get('userInfo');

    console.log(`post`, post);

    return (
      <Spin spinning={loading}>
        <Row type="flex" justify="center" className="post-detail-layout">
          <Col xl={14} lg={16} md={18} sm={20} xs={22}>
            <Card
              className="post-detail-card"
              cover={
                <img
                  className="post-detail-image"
                  alt="example"
                  src={`${(post && post.image_urls[0] && `${SERVER_URI}${post.image_urls[0]}`) || ''}`}
                />
              }
              actions={[
                <Icon
                  key={1}
                  type="delete"
                  onClick={this.handleDeleteConfirm}
                  style={{
                    visibility: isLoggedIn ? 'visible' : 'hidden',
                  }}
                />,
                <Icon
                  key={2}
                  type="edit"
                  onClick={this.showUpdatePostForm}
                  style={{
                    visibility: isLoggedIn ? 'visible' : 'hidden',
                  }}
                />,
              ]}
            >
              <Card.Meta
                avatar={
                  <Avatar
                    src={`${(post && post.author && post.author.avatar) ||
                      'https://1.bp.blogspot.com/-M3rWW8Xl4jo/W04XYb__5UI/AAAAAAAl6tg/AOJxsf57WWIufWS-22vebEs-OzOYcWZHACLcBGAs/s1600/AW1372610_01.gif'}`}
                  />
                }
                title={
                  <div className="post-detail-meta-title">{`${(post && post.author && post.author.name) || ''}`}</div>
                }
                description={`Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeb`}
              />
              <Title className="post-detail-title">{`${(post && post.title) || ''}`}</Title>
              <Paragraph className="post-detail-content">{`${(post && post.content) || ''}`}</Paragraph>
            </Card>
            <UpdatePostForm
              postData={{
                id: (post && post._id) || '',
                title: (post && post.title) || '',
                content: (post && post.content) || '',
                image_urls: (post && post.image_urls) || [''],
              }}
              visible={visibleUpdatePostForm}
              onHide={this.hideUpdatePostForm}
              onSuccess={this.handleUpdatePostSuccessfully}
            />
          </Col>
        </Row>
      </Spin>
    );
  }
}

export default withRouter(PostDetail);
