import React, { Component, createRef } from 'react';
import { Button, Modal, notification } from 'antd';
import { updatePost } from 'services/Home/Posts';
import { RequestData as UpdatePostParams } from 'services/Home/Posts/updatePost';
import PostForm from '../../components/PostForm';
import { Post } from 'types';
import { SERVER_URI } from 'constants/index';

interface PostData {
  id: Post['_id'];
  title: Post['title'];
  content: Post['content'];
  image_urls: Post['image_urls'];
}

interface Props {
  postData: PostData;
  visible: boolean;
  onHide: () => void;
  onSuccess: () => void;
}

interface State {
  loading: boolean;
}

class UpdatePostForm extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  formRef: any = createRef();

  componentWillReceiveProps(nextProps: Props) {
    console.log(`next props`, nextProps);

    if (nextProps.visible) {
      const { image_urls, title, content } = this.props.postData;

      console.log(`formRef`, this.formRef);

      this.formRef.props.form.setFieldsValue({
        title,
        content,
        upload: image_urls.map((imageUrl, index) => {
          return {
            uid: `${index}`,
            name: 'image.png',
            status: 'done',
            response: {
              data: {
                paths: `${imageUrl}`,
              },
            },
            url: `${SERVER_URI}${imageUrl}`,
          };
        }),
      });
    }
  }

  onCancel = () => {
    this.props.onHide();
    this.formRef.props.form.resetFields();
  };

  doUpdatePost = async (values: UpdatePostParams) => {
    try {
      this.setState({
        loading: true,
      });

      const result = await updatePost(values);

      if (result.err !== 0) {
        notification.error({
          message: 'Something went wrong, please try again!',
          description: `${result.msg.toString()}`,
        });

        return;
      }

      notification.success({
        message: 'Update post successfully!',
        description: `${result.msg}`,
      });

      this.props.onSuccess();
      this.formRef.props.form.resetFields();
    } catch (error) {
      notification.error({
        message: 'Failed to update post',
        description: `${error.toString()}`,
      });
    } finally {
      this.setState({
        loading: false,
      });
    }
  };

  onSubmit = () => {
    this.formRef.props.form.validateFields((error: any, fieldValues: any) => {
      if (!error) {
        console.log(`form value`, fieldValues);

        const values: UpdatePostParams = {
          _id: this.props.postData.id,
          title: fieldValues.title,
          content: fieldValues.content,
          image_urls: fieldValues.upload[0].response.data.paths,
        };

        this.doUpdatePost(values);
      }
    });
  };

  saveFormRef = (formRef: any) => {
    this.formRef = formRef;
  };

  render() {
    const { visible } = this.props;

    return (
      <Modal
        visible={visible}
        title="Update post"
        onCancel={this.onCancel}
        centered
        maskClosable={false}
        forceRender
        footer={[
          <Button key="back" onClick={this.onCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={this.onSubmit}>
            {`Update`}
          </Button>,
        ]}
      >
        <PostForm submitting={this.state.loading} wrappedComponentRef={this.saveFormRef} />
      </Modal>
    );
  }
}

export default UpdatePostForm;
