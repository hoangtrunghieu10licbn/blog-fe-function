import React, { Component } from 'react';
import QueueAnim from 'rc-queue-anim';
import PostDetail from './components/PostDetail';

class Page extends Component {
  render() {
    return (
      <QueueAnim type={['right', 'left']}>
        <div key="0">
          <PostDetail />
        </div>
      </QueueAnim>
    );
  }
}

export default Page;
