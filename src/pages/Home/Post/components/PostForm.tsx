import React, { Component } from 'react';
import { Button, Form, Input, Upload, Icon } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { UploadProps } from 'antd/lib/upload';
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import { Cookies } from 'react-cookie';
import { UPLOAD_URL, SERVER_URI } from 'constants/index';
import { Post } from 'types';

const cookies = new Cookies();

const { TextArea } = Input;
interface Props extends FormComponentProps {
  postData?: {
    title: Post['title'];
    content: Post['content'];
    image_urls: Post['image_urls'];
  };
  submitting: boolean;
}

class PostForm extends Component<Props, any> {
  handleChangeEvent = (info: UploadChangeParam) => {
    console.log(`info`, info);

    let fileList = [...info.fileList];

    fileList = fileList.slice(-1);

    fileList = fileList.map((file: UploadFile) => {
      if (file.response) {
        // Component will show file.url as link
        file.url = `${SERVER_URI}${(file.response.data && file.response.data.paths) || ''}`;
      }
      return file;
    });

    console.log(`file list`, fileList);

    return fileList;
  };

  render() {
    const { submitting, form } = this.props;

    const { getFieldDecorator } = form;

    const uploadProps: UploadProps = {
      name: 'upload',
      action: UPLOAD_URL,
      headers: {
        access_token: cookies.get('accessToken'),
      },
      listType: 'picture',
    };

    return (
      <Form>
        <Form.Item label="Title">
          {getFieldDecorator('title', {
            rules: [
              {
                whitespace: true,
                required: true,
                message: 'Please input your title!',
              },
              {
                max: 100,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<Input placeholder="Title" />)}
        </Form.Item>
        <Form.Item label="Content">
          {getFieldDecorator('content', {
            rules: [
              {
                whitespace: true,
                required: true,
                message: 'Please input your content!',
              },
              {
                max: 5000,
                message: 'Character number is greater than allowed!',
              },
            ],
          })(<TextArea placeholder="Content" autosize={{ minRows: 3, maxRows: 8 }} />)}
        </Form.Item>
        <Form.Item label="Upload">
          {getFieldDecorator('upload', {
            valuePropName: 'fileList',
            getValueFromEvent: this.handleChangeEvent,
            rules: [
              {
                required: true,
                message: 'Please upload your image!',
              },
            ],
          })(
            <Upload {...uploadProps}>
              <Button loading={submitting}>
                <Icon type="upload" /> Click to upload
              </Button>
            </Upload>,
          )}
        </Form.Item>
      </Form>
    );
  }
}

const WrappedPostForm = Form.create<Props>({
  name: 'post-form',
})(PostForm);

export default WrappedPostForm;
