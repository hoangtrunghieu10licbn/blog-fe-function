import React, { Component, createRef } from 'react';
import { Button, Modal, notification } from 'antd';
import { addPost } from 'services/Home/Posts';
import { RequestData as AddPostParams } from 'services/Home/Posts/addPost';
import PostForm from '../../components/PostForm';

interface Props {
  visible: boolean;
  onHide: () => void;
  onSuccess: () => void;
}

interface State {
  loading: boolean;
}

class AddPostForm extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  formRef: any = createRef();

  onCancel = () => {
    this.props.onHide();
    this.formRef.props.form.resetFields();
  };

  doAddPost = async (values: AddPostParams) => {
    try {
      this.setState({
        loading: true,
      });

      const result = await addPost(values);

      if (result.err !== 0) {
        notification.error({
          message: 'Something went wrong, please try again!',
          description: `${result.msg.toString()}`,
        });

        return;
      }

      notification.success({
        message: 'Create post successfully!',
        description: `${result.msg}`,
      });

      this.props.onSuccess();
      this.formRef.props.form.resetFields();
    } catch (error) {
      notification.error({
        message: 'Failed to create post',
        description: `${error.toString()}`,
      });
    } finally {
      this.setState({
        loading: false,
      });
    }
  };

  onSubmit = () => {
    this.formRef.props.form.validateFields((error: any, fieldValues: any) => {
      if (!error) {
        console.log(`form value`, fieldValues);

        const values: AddPostParams = {
          title: fieldValues.title,
          content: fieldValues.content,
          image_urls: fieldValues.upload[0].response.data.paths,
        };

        this.doAddPost(values);
      }
    });
  };

  saveFormRef = (formRef: any) => {
    this.formRef = formRef;
  };

  render() {
    const { visible } = this.props;

    return (
      <Modal
        visible={visible}
        title="Create post"
        onCancel={this.onCancel}
        centered
        forceRender
        maskClosable={false}
        footer={[
          <Button key="back" onClick={this.onCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={this.onSubmit}>
            {`Create`}
          </Button>,
        ]}
      >
        <PostForm submitting={this.state.loading} wrappedComponentRef={this.saveFormRef} />
      </Modal>
    );
  }
}

export default AddPostForm;
