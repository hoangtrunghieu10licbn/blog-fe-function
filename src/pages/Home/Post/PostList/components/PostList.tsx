import React, { Component } from 'react';
import { List, Row, Col, Button, notification, Spin } from 'antd';
import PostItem from './PostItem';
import { MdAddCircle } from 'react-icons/md';
import { getPostList } from 'services/Home/Posts';
import { Post } from 'types';
import { Cookies } from 'react-cookie';
import AddPostForm from './AddPostForm';
import './PostList.css';

const cookies = new Cookies();

const pageSize = 5;

interface State {
  loading: boolean;
  total: number;
  posts: Post[];
  visibleAddPostForm: boolean;
}

class PostList extends Component<any, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      loading: false,
      total: 0,
      posts: [],
      visibleAddPostForm: false,
    };
  }

  componentDidMount() {
    this.doGetPostList();
  }

  doGetPostList = async () => {
    try {
      this.setState({
        loading: true,
      });

      const result = await getPostList({
        limit: pageSize,
        offset: 0,
      });

      console.log(`result`, result);

      if (result.err !== 0) {
        notification.error({
          message: 'Something went wrong!',
          description: `${result.msg}`,
        });
        return;
      }

      this.setState({
        posts: result.data.posts,
        total: result.data.total,
      });
    } catch (e) {
      notification.error({
        message: `Something went wrong!`,
        description: `${e.toString()}`,
      });
    } finally {
      this.setState({
        loading: false,
      });
    }
  };

  showAddPostForm = () => {
    this.setState({
      visibleAddPostForm: true,
    });
  };

  hideAddPostForm = () => {
    this.setState({
      visibleAddPostForm: false,
    });
  };

  handleAddPostSuccess = () => {
    this.doGetPostList();
    this.hideAddPostForm();
  };

  render() {
    const isLoggedIn = !!cookies.get('userInfo');

    return (
      <div className="post-list">
        {isLoggedIn && (
          <Row type="flex" justify="center">
            <Col xl={14} lg={18} md={20} sm={24} xs={24} className="button-container">
              <Button type="primary" className="button" onClick={this.showAddPostForm}>
                <MdAddCircle className="btn-icon" />
                {`Create post`}
              </Button>
            </Col>
          </Row>
        )}

        <Row type="flex" justify="center">
          <Col xl={14} lg={18} md={20} sm={24} xs={24}>
            <Spin spinning={this.state.loading}>
              <List
                itemLayout="vertical"
                size="large"
                pagination={{
                  onChange: page => {
                    console.log(page);
                  },
                  pageSize: pageSize,
                }}
                dataSource={this.state.posts}
                renderItem={(post, index) => (
                  <div key={`${index}`}>
                    <PostItem
                      id={post._id}
                      className="post-item"
                      title={post.title}
                      content={post.content}
                      image={post.image_urls[0]}
                      author={post.author}
                    />
                  </div>
                )}
              />
            </Spin>
          </Col>
        </Row>
        <AddPostForm
          visible={this.state.visibleAddPostForm}
          onHide={this.hideAddPostForm}
          onSuccess={this.handleAddPostSuccess}
        />
      </div>
    );
  }
}

export default PostList;
