import React, { Component } from 'react';
import { Layout, Row, Col, Button, Icon, Avatar } from 'antd';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Cookies } from 'react-cookie';
import './Header.css';

const cookies = new Cookies();

const { Header } = Layout;

interface State {
  isLogin: boolean;
}

class BlogHeader extends Component<RouteComponentProps, State> {
  constructor(props: RouteComponentProps) {
    super(props);
    this.state = {
      isLogin: !!cookies.get('userInfo'),
    };
  }

  handleLogout = () => {
    cookies.set('userInfo', '');
    cookies.set('accessToken', '');
    localStorage.removeItem('userInfo');
    localStorage.removeItem('accessToken');

    this.setState({
      isLogin: false,
    });

    window.location.reload();

    // this.props.history.push('/home');
  };

  render() {
    const { isLogin } = this.state;

    return (
      <Header className="header">
        <Row type="flex" justify="space-between">
          <Link to="/home" className="button-blog">
            BLOG
          </Link>
          {isLogin ? (
            <Col span={4} className="button-block-loggedin">
              <Avatar size="large" icon="user" />
              <Button onClick={this.handleLogout}>
                <Icon type="logout" />
                {`Log out`}
              </Button>
            </Col>
          ) : (
            <Col span={4} className="button-block">
              <Button>
                <Link to="/login">{`Login`}</Link>
              </Button>
              <Button>
                <Link to="/register">{`Register`}</Link>
              </Button>
            </Col>
          )}
        </Row>
      </Header>
    );
  }
}

export default withRouter(BlogHeader);
